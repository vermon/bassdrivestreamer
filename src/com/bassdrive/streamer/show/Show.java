package com.bassdrive.streamer.show;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bassdrive.streamer.ArchiveListLoader;
import com.bassdrive.streamer.PlayerService;
import com.bassdrive.streamer.ArchiveFragment.OnArchiveFileSelectedListener;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Show extends ArchiveItem implements Playable {

	private static final Pattern p = Pattern
			.compile(".*\\[(.*)\\] (.*) - (.*)\\.mp3");

	public Show(Parcel p) {
		super(p);
	}

	public Show(String url) {
		super(url);
	}

	@Override
	public void handleClick(ArchiveListLoader loader,
			OnArchiveFileSelectedListener listener) {
		Log.d("ArchiveShow", "Selecting archive show " + url);
		PlayerService.play(loader.getContext(), this);
		listener.onSelected(this);
		loader.setActive(null);
	}

	@Override
	protected Info getInfo() {
		String info = escape(url);
		Matcher m = p.matcher(info);
		if (m.matches()) {
			return new Info(m.group(1), m.group(2), m.group(3));
		}
		return new Info(url);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(url);
	}

	public static final Parcelable.Creator<Show> CREATOR = new Parcelable.Creator<Show>() {
		@Override
		public Show createFromParcel(Parcel in) {
			try {
				return new Show(in);
			} catch (Exception e) {
				throw new RuntimeException(
						"Unable to create ArchiveShow from parcel", e);
			}
		}

		@Override
		public Show[] newArray(int size) {
			return new Show[size];
		}
	};

	@Override
	public boolean isLive() {
		return false;
	}

	@Override
	public String toString() {
		return getInfo().toString();
	}

}
