package com.bassdrive.streamer.show;

public interface Item {

	public interface OnInfoCallback {
		void onInfoCallback(Info info);
	}

	String getURL();

	void getInfo(OnInfoCallback onInfoCallback);

}