package com.bassdrive.streamer.show;

import android.annotation.SuppressLint;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public interface Playable extends Parcelable, Item {

	boolean isLive();

}
