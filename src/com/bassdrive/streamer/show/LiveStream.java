package com.bassdrive.streamer.show;

import net.sourceforge.servestream.media.ShoutCastMetadataRetriever;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;

public class LiveStream implements Playable {

	private static final String LIVE_URL = "http://shouthost.com.18.streams.bassdrive.com:8398";

	@Override
	public String getURL() {
		return LIVE_URL;
	}

	@Override
	public void getInfo(OnInfoCallback onInfoCallback) {
		new RetrieveTrackInfoAsyncTask(onInfoCallback).execute();
	}

	@Override
	public boolean isLive() {
		return true;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
	}

	public static final Parcelable.Creator<LiveStream> CREATOR = new Parcelable.Creator<LiveStream>() {
		@Override
		public LiveStream createFromParcel(Parcel in) {
			return new LiveStream();
		}

		@Override
		public LiveStream[] newArray(int size) {
			return new LiveStream[size];
		}
	};

	private class RetrieveTrackInfoAsyncTask extends
			AsyncTask<Void, Integer, Info> {

		private final OnInfoCallback callback;

		public RetrieveTrackInfoAsyncTask(OnInfoCallback callback) {
			this.callback = callback;
		}

		@Override
		protected Info doInBackground(Void... params) {
			ShoutCastMetadataRetriever smr = new ShoutCastMetadataRetriever();
			try {
				smr.setDataSource(LIVE_URL.toString());
				String artist = smr
						.extractMetadata(ShoutCastMetadataRetriever.METADATA_KEY_ARTIST);
				String title = smr
						.extractMetadata(ShoutCastMetadataRetriever.METADATA_KEY_TITLE);
				return new Info(title, artist);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new Info("Bassdrive", "Live");
		}

		@Override
		protected void onPostExecute(Info result) {
			callback.onInfoCallback(result);
		}

	}

}
