package com.bassdrive.streamer.show;

import java.io.Serializable;

import org.jsoup.helper.StringUtil;

public class Info implements Serializable {
	private static final long serialVersionUID = 1L;

	private String date;

	private final String title;

	private String artist;

	public Info(String title) {
		this.title = title;
	}

	public Info(String show, String artist) {
		this.title = show;
		this.artist = artist;
	}

	public Info(String date, String show, String artist) {
		this(show, artist);
		this.date = date;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (!StringUtil.isBlank(date)) {
			builder.append(date + " - ");
		}
		if (!StringUtil.isBlank(title)) {
			builder.append(title);
		}
		if (!StringUtil.isBlank(artist)) {
			if (!StringUtil.isBlank(title)) {
				builder.append(" - ");
			}
			builder.append(artist);
		}
		return builder.toString();
	}
}
