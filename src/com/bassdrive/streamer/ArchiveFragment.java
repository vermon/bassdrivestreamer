package com.bassdrive.streamer;

import java.net.URISyntaxException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.app.SherlockListFragment;
import com.bassdrive.streamer.show.ArchiveItem;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EFragment;
import com.googlecode.androidannotations.annotations.FragmentArg;
import com.googlecode.androidannotations.annotations.ItemClick;

@EFragment
public class ArchiveFragment extends SherlockListFragment implements LoaderManager.LoaderCallbacks<List<ArchiveItem>> {

	public interface OnArchiveFileSelectedListener {
		public void onSelected(ArchiveItem item);

		public Context getApplicationContext();
	}

	private static final String TAG = "ArchiveLoader";

	public static final String ARCHIVE_POSITION = "archive.position";

	private ArrayAdapter<ArchiveItem> shows;

	@FragmentArg(ARCHIVE_POSITION)
	int position;

	private OnArchiveFileSelectedListener mListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnArchiveFileSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnArchiveFileSelectedListener");
		}
	}

	@AfterViews
	void init() {
		Log.d("Archive", "Showing position " + position);
		this.shows = new ArrayAdapter<ArchiveItem>(getActivity(), android.R.layout.simple_list_item_1);
		setListAdapter(shows);
		getLoaderManager().initLoader(position, null, this);
	}

	@ItemClick
	void listItemClicked(int ind) {
		Loader<List<ArchiveItem>> loader = getLoaderManager().getLoader(position);
		ArchiveListLoader showListLoader = (ArchiveListLoader) loader;
		ArchiveItem show = showListLoader.getItem(ind);
		show.handleClick(showListLoader, mListener);
		showListLoader.onContentChanged();
		Log.d(TAG, "Clicked");
	}

	@Override
	public Loader<List<ArchiveItem>> onCreateLoader(int id, Bundle bundle) {
		Log.d(TAG, "Creating new loader");
		try {
			return new ArchiveListLoader(getActivity(), position);
		} catch (URISyntaxException e) {
			throw new RuntimeException("Invalid base URI", e);
		}
	}

	@Override
	public void onLoadFinished(Loader<List<ArchiveItem>> loader, List<ArchiveItem> data) {
		Log.d(TAG, "Loading finished");
		shows.clear();
		for (ArchiveItem archiveItem : data) {
			shows.add(archiveItem);
		}
	}

	@Override
	public void onLoaderReset(Loader<List<ArchiveItem>> loader) {
		shows.clear();
	}

}
